package info.amber.vboot.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book")
public class BookController {
    @Value("${book.name}")
    private String bookName;

    @Value(value = "${book.author}")
    private String bookAuthor;

    @Value("${book.pinyin}")
    private String bookPinYin;

    @RequestMapping(value = {"/","*"}, produces = "text/plain;charset=UTF-8")
    public String index() {
//        return "Hello Spring Boot!";
        return "Hello Spring Boot! The BookName is " + bookName + ";and Book Author is " + bookAuthor + ";and Book PinYin is " + bookPinYin;
    }

}






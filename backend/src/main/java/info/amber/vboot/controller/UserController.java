package info.amber.vboot.controller;

import info.amber.vboot.model.User;
import info.amber.vboot.service.UserService;
import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vo.UserVO;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    protected MapperFacade mapper;

    @Autowired
    private UserService userService;

    @RequestMapping("find")
    public Object find() {
        User user = new User();
        user.setId(1L);
        user.setUsername("张三");
        user.setAge(25);
        user.setSex(1);

        return user;
    }

    @RequestMapping("get/{name}")
    public Map<String, Object> get(@PathVariable String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("username", name);
        map.put("value", "hello world");
        return map;

    }
    @RequestMapping("get/{id}")
    @ResponseBody
    public Object get(HttpServletRequest request, @PathVariable Long id) {
        LOGGER.info("id:{}", id);
        User user = userService.getUser(id);
        return mapper.map(user, UserVO.class);
    }

    @GetMapping("findPage")
    @ResponseBody
    public Object findPage(int page, int size, String username) {
        try {
            return userService.findPage(page, size, username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "getUsername")
    public String getUsername(String callback) {
        return "admin";
    }
}

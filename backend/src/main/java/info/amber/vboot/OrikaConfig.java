package info.amber.vboot;


import info.amber.vboot.model.User;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vo.UserVO;

@Configuration
public class OrikaConfig {
    @Bean
    public ConfigurableMapper configurableMapper() {
        return new ConfigurableMapper() {
            @Override
            public void configure(MapperFactory factory) {
                factory.registerClassMap(factory.classMap(User.class, UserVO.class)
                        .field("username", "name")
                        .exclude("phone")
                        .byDefault()
                        .toClassMap());

//                factory.getConverterFactory().registerConverter(new MyEnumConverter());
            }
        };

    }

//    @Bean
//    public MapperFacade mapper() {
//        MapperFactory factory = new DefaultMapperFactory.Builder().build();
//        factory.classMap(User.class, UserVO.class)
//                .exclude("phone")
//                .field("username", "name").byDefault().register();
//
//        MapperFacade mapper = factory.getMapperFacade();
//        return mapper;
//    }
}

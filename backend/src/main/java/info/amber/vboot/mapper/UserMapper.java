package info.amber.vboot.mapper;

import info.amber.vboot.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper extends RootMapper {
    @Select("select  * from sys_user where username =#{username}")
    List<User> findUsersByUsername(String username);

    @Delete({
            "delete from sys_user",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
            "insert into sys_user (id, username, ",
            "sex, create_time, ",
            "age, password, phone)",
            "values (#{id,jdbcType=BIGINT}, #{username,jdbcType=VARCHAR}, ",
            "#{sex,jdbcType=INTEGER}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{age,jdbcType=INTEGER}, #{password,jdbcType=VARCHAR}, #{phone,jdbcType=VARCHAR})"
    })
    int insert(User record);

    int insertSelective(User record);

    @Select({
            "select",
            "id, username, sex, create_time, age, password, phone",
            "from sys_user",
            "where id = #{id,jdbcType=BIGINT}"
    })
    @ResultMap("info.amber.vboot.mapper.UserMapper.BaseResultMap")
    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    @Update({
            "update sys_user",
            "set username = #{username,jdbcType=VARCHAR},",
            "sex = #{sex,jdbcType=INTEGER},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "age = #{age,jdbcType=INTEGER},",
            "password = #{password,jdbcType=VARCHAR},",
            "phone = #{phone,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(User record);


}
package info.amber.vboot.mapper;

import info.amber.vboot.model.City;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

@Component
public class CityMapper {

    private final SqlSession sqlSession;


    public CityMapper(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    public City selectCityById(Long id){
        return  this.sqlSession.selectOne("selectCityById",id);
    }

}

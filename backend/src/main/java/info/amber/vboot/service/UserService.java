package info.amber.vboot.service;

import com.github.pagehelper.PageInfo;
import info.amber.vboot.model.User;

public interface UserService {


    PageInfo findPage(int page, int size, String username);

    User getUser(Long id);

}

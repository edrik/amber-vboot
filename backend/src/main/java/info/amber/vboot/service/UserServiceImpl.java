package info.amber.vboot.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import info.amber.vboot.mapper.UserMapper;
import info.amber.vboot.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    protected UserMapper userMapper;


    @Override
    public PageInfo findPage(int page, int size, String username) {
        try {
            PageHelper.startPage(page, size);
            List<User> userList = userMapper.findUsersByUsername(username);
            PageInfo<User> pageInfo = new PageInfo<>(userList);
            return pageInfo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUser(Long id) {

        return userMapper.selectByPrimaryKey(id);
    }


}

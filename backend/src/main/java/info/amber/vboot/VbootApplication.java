package info.amber.vboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


@EnableAutoConfiguration
@ComponentScan
@MapperScan("info.amber.vboot.mapper")
@ComponentScan(basePackages = {"info.amber.vboot"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class VbootApplication extends SpringBootServletInitializer {
    @Override

    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(VbootApplication.class);
    }

    public static void main(String[] args) {

//        SpringApplicationBuilder builder = new SpringApplicationBuilder(SbootVueApplication.class);
//        //修改Banner的模式为OFF
//        builder.bannerMode(Banner.Mode.OFF).run(args);
        SpringApplication.run(VbootApplication.class, args);
    }

}
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/pages/HelloWorld'
import Main from '@/pages/Main'
import LoginForm from '@/pages/LoginForm';



Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {name: '/', path: '/', redirect: '/main'},
    {name: 'Main', path: '/main', component: Main},
    {name: 'Login', path: '/login', component: LoginForm},
    {name: 'Hello', path: '/hello', component: require('../pages/hello.vue'), meta: {}}

  ]
})

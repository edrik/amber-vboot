

@echo off
set _dir=%cd%

echo %_dir%
cd frontend

call npm run build

cd %_dir%
cd backend
cd
call mvnw package
cd %_dir%
@echo on
